/**
* Задание 12 - Создать интерфейс StarWars DB для данных из SWAPI.
*
* Используя SWAPI, вывести информацию по всем планетам с пагинацией и возможностью просмотреть доп.
* информацию в модальном окне с дозагрузкой смежных ресурсов из каждой сущности.
*
* Данные для отображения в карточке планеты:
* 1. Наименование (name)
* 2. Диаметр (diameter)
* 3. Население (population)
* 4. Уровень гравитации (gravity)
* 5. Природные зоны (terrain)
* 6. Климатические зоны (climate)
*
* При клике по карточке отображаем в модальном окне всю информацию
* из карточки, а также дополнительную:
* 1. Список фильмов (films)
* - Номер эпизода (episode_id)
* - Название (title)
* - Дата выхода (release_date)
* 2. Список персонажей
* - Имя (name)
* - Пол (gender)
* - День рождения (birth_year)
* - Наименование родного мира (homeworld -> name)
*
* Доп. требования к интерфейсу:
* 1. Выводим 10 карточек на 1 странице
* 2. Пагинация позволяет переключаться между страницами,
* выводить общее количество страниц и текущие выбранные
* элементы в формате 1-10/60 для 1 страницы или 11-20/60 для второй и т.д.
* 3. Используем Bootstrap 4 для создания интерфейсов.
* 4. Добавить кнопку "Показать все" - по клику загрузит
* все страницы с планетами и выведет
* информацию о них в един
*/
const urls = [
    'http://swapi.dev/api/planets/?page=1',
    'http://swapi.dev/api/planets/?page=2',
    'http://swapi.dev/api/planets/?page=3',
    'http://swapi.dev/api/planets/?page=4',
    'http://swapi.dev/api/planets/?page=5',
    'http://swapi.dev/api/planets/?page=6',
];
const navigatorEl = document.querySelector('.navigation-container');

(async () => {
    const getPage = async (url = urls[0]) => {
                
        const response = await fetch(url);
        const data = await response.json();
        const planets = data.results;
                        
        for (let i = 0; i < planets.length; i++) {
            const plName = planets[i].name;
            const plDiameter = planets[i].diameter;
            const plPopulation = planets[i].population;
            const plGravity = planets[i].gravity;
            const plTerrain = planets[i].terrain;
            const plClimate = planets[i].climate;
            const filmsRequest = planets[i].films.map(
                filmUrl => fetch(filmUrl).then(res => res.json())
                    .then(film => `Episode ${film.episode_id}. ${film.title} (${film.release_date})`)
            )
            const plFilmList = await Promise
                .all(filmsRequest)
                .then(res => res.join(';<br>'));
            
                       
            const getHomeworld = async url => {
                const response = await fetch(url);
                const data = await response.json();
                const homeworld = data.name;
                return homeworld;
            }            
            const residentsRequest = planets[i].residents.map(
                residentUrl => fetch(residentUrl)
                    .then(res => res.json())
                    .then(resident => getHomeworld(resident.homeworld)
                        .then(homeworld => `
                            ${resident.name}:
                            gender: ${resident.gender},
                            birth year: ${resident.birth_year},
                            homeworld: ${homeworld}`)
                    )
            )
            const plResidents = await Promise
                .all(residentsRequest)
                .then(res => res.join(';<br>'));
                
            const containerEl = document.querySelector('.cards-container');
            const newCard = document.createElement('div');
            newCard.classList.add('col-sm-4');
            newCard.innerHTML = `
            <div class="card m-2">
                <img src="img/nasa-rTZW4f02zY8-unsplash.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">${plName}</h5>
                    <p class="card-text">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">diameter: ${plDiameter}</li>
                        <li class="list-group-item">population: ${plPopulation}</li>
                        <li class="list-group-item">gravity: ${plGravity}</li>
                        <li class="list-group-item">terrain: ${plTerrain}</li>
                        <li class="list-group-item">climate: ${plClimate}</li>
                    </ul>
                    </p>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#q${plDiameter}">
                      Learn more
                    </button>
                </div>
                <!-- Modal -->
    <div class="modal fade" id="q${plDiameter}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">${plName}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <img src="img/nasa-rTZW4f02zY8-unsplash.jpg" class="card-img-top" alt="...">
                    <p class="card-text">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">diameter: ${plDiameter}</li>
                        <li class="list-group-item">population: ${plPopulation}</li>
                        <li class="list-group-item">gravity: ${plGravity}</li>
                        <li class="list-group-item">terrain: ${plTerrain}</li>
                        <li class="list-group-item">climate: ${plClimate}</li>
                        <li class="list-group-item">films: ${plFilmList}</li>
                        <li class="list-group-item">residents: ${plResidents}</li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>
            </div>`;
            containerEl.appendChild(newCard);            
        }
        const pageNum = urls.indexOf(url) + 1;
        const pageId = () => {
            switch (pageNum) {                
                case 2:
                    return '11-20/60';
                case 3:
                    return '21-30/60';
                case 4:
                    return '31-40/60';
                case 5:
                    return '41-50/60';
                case 6:
                    return '51-60/60';
                default:
                    return '1-10/60';
            }
        };
        const newNav = document.createElement('nav');
        newNav.setAttribute('aria-label', 'Page navigation example');
        newNav.innerHTML = `
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" mode="${pageNum - 1}" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li class="page-item"><a class="page-link" href="#">${pageId()}</a></li>
            <li class="page-item">
                <a class="page-link" mode="${pageNum + 1}" aria-label="Next">
                    &raquo;
                </a>
            </li>
            <li class="page-item">
                <a class="page-link" mode="all" aria-label="Next">
                    Show all
                </a>
            </li>
        </ul>`;
        navigatorEl.appendChild(newNav);
        if (pageNum === 1) {
            document.querySelector('.pagination li').classList.add('disabled')
        }
        if (pageNum === 6) {
            document.querySelectorAll('.pagination li')[2].classList.add('disabled')
        }
    }
    
    return await getPage();
})();


navigatorEl.addEventListener(
    'click',
    e => {
        if (!e.target.hasAttribute('mode')) {
            return;
        }
        if (e.target.getAttribute('mode') === 0 && e.target.getAttribute('mode') === 7) {
            return;
        }
        const urlIndex = Number(e.target.getAttribute('mode') - 1);
        const containerEl = document.querySelector('.cards-container');
        containerEl.textContent = '';        
        if (e.target.getAttribute('mode') === 'all') {            
        (async () => {
            const getPage = async () => {
                navigatorEl.textContent = '';
                for (let j = 0; j < urls.length; j++) {
                    const response = await fetch(urls[j]);
                    const data = await response.json();
                    const planets = data.results;
                    
                    for (let i = 0; i < planets.length; i++) {
                        const plName = planets[i].name;
                        const plDiameter = planets[i].diameter;
                        const plPopulation = planets[i].population;
                        const plGravity = planets[i].gravity;
                        const plTerrain = planets[i].terrain;
                        const plClimate = planets[i].climate;
                        const filmsRequest = planets[i].films.map(
                            filmUrl => fetch(filmUrl).then(res => res.json())
                                .then(film => `Episode ${film.episode_id}. ${film.title} (${film.release_date})`)
                        )
                        const plFilmList = await Promise
                            .all(filmsRequest)
                            .then(res => res.join(';<br>'));
                        

                        const getHomeworld = async url => {
                            const response = await fetch(url);
                            const data = await response.json();
                            const homeworld = data.name;
                            return homeworld;
                        }            
                        const residentsRequest = planets[i].residents.map(
                            residentUrl => fetch(residentUrl)
                                .then(res => res.json())
                                .then(resident => getHomeworld(resident.homeworld)
                                    .then(homeworld => `
                                        ${resident.name}:
                                        gender: ${resident.gender},
                                        birth year: ${resident.birth_year},
                                        homeworld: ${homeworld}`)
                                )
                        )
                        const plResidents = await Promise
                            .all(residentsRequest)
                            .then(res => res.join(';<br>'));
                        
                        const newCard = document.createElement('div');
                        newCard.classList.add('col-sm-4');
                        newCard.innerHTML = `
                        <div class="card m-2">
                            <img src="img/nasa-rTZW4f02zY8-unsplash.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">${plName}</h5>
                                <p class="card-text">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">diameter: ${plDiameter}</li>
                                    <li class="list-group-item">population: ${plPopulation}</li>
                                    <li class="list-group-item">gravity: ${plGravity}</li>
                                    <li class="list-group-item">terrain: ${plTerrain}</li>
                                    <li class="list-group-item">climate: ${plClimate}</li>
                                </ul>
                                </p>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#q${j}${i}">
                                  Learn more
                                </button>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="q${j}${i}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="staticBackdropLabel">${plName}</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <img src="img/nasa-rTZW4f02zY8-unsplash.jpg" class="card-img-top" alt="...">
                                            <p class="card-text">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">diameter: ${plDiameter}</li>
                                                <li class="list-group-item">population: ${plPopulation}</li>
                                                <li class="list-group-item">gravity: ${plGravity}</li>
                                                <li class="list-group-item">terrain: ${plTerrain}</li>
                                                <li class="list-group-item">climate: ${plClimate}</li>
                                                <li class="list-group-item">films: ${plFilmList}</li>
                                                <li class="list-group-item">residents: ${plResidents}</li>
                                            </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;
                        containerEl.appendChild(newCard);            
                    }
                }
                
            }
            return await getPage();
        })();
        } else {
            (async () => {
                const getPage = async (url) => {
                    const response = await fetch(url);
                    const data = await response.json();
                    const planets = data.results;

                    for (let i = 0; i < planets.length; i++) {
                        const plName = planets[i].name;
                        const plDiameter = planets[i].diameter;
                        const plPopulation = planets[i].population;
                        const plGravity = planets[i].gravity;
                        const plTerrain = planets[i].terrain;
                        const plClimate = planets[i].climate;
                        const filmsRequest = planets[i].films.map(
                            filmUrl => fetch(filmUrl).then(res => res.json())
                                .then(film => `Episode ${film.episode_id}. ${film.title} (${film.release_date})`)
                        )
                        const plFilmList = await Promise
                            .all(filmsRequest)
                            .then(res => res.join(';<br>'));
                        

                        const getHomeworld = async url => {
                            const response = await fetch(url);
                            const data = await response.json();
                            const homeworld = data.name;
                            return homeworld;
                        }            
                        const residentsRequest = planets[i].residents.map(
                            residentUrl => fetch(residentUrl)
                                .then(res => res.json())
                                .then(resident => getHomeworld(resident.homeworld)
                                    .then(homeworld => `
                                        ${resident.name}:
                                        gender: ${resident.gender},
                                        birth year: ${resident.birth_year},
                                        homeworld: ${homeworld}`)
                                )
                        )
                        const plResidents = await Promise
                            .all(residentsRequest)
                            .then(res => res.join(';<br>'));
                        
                        const newCard = document.createElement('div');
                        newCard.classList.add('col-sm-4');
                        newCard.innerHTML = `
                        <div class="card m-2">
                            <img src="img/nasa-rTZW4f02zY8-unsplash.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">${plName}</h5>
                                <p class="card-text">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">diameter: ${plDiameter}</li>
                                    <li class="list-group-item">population: ${plPopulation}</li>
                                    <li class="list-group-item">gravity: ${plGravity}</li>
                                    <li class="list-group-item">terrain: ${plTerrain}</li>
                                    <li class="list-group-item">climate: ${plClimate}</li>
                                </ul>
                                </p>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#q${i}">
                                  Learn more
                                </button>
                            </div>
                            <!-- Modal -->
                <div class="modal fade" id="q${i}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticBackdropLabel">${plName}</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <img src="img/nasa-rTZW4f02zY8-unsplash.jpg" class="card-img-top" alt="...">
                                <p class="card-text">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">diameter: ${plDiameter}</li>
                                    <li class="list-group-item">population: ${plPopulation}</li>
                                    <li class="list-group-item">gravity: ${plGravity}</li>
                                    <li class="list-group-item">terrain: ${plTerrain}</li>
                                    <li class="list-group-item">climate: ${plClimate}</li>
                                    <li class="list-group-item">films: ${plFilmList}</li>
                                    <li class="list-group-item">residents: ${plResidents}</li>
                                </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                        </div>`;
                        containerEl.appendChild(newCard);            
                    }
                    const pageNum = urls.indexOf(url) + 1;
                    const pageId = () => {
                        switch (pageNum) {                            
                            case 2:
                                return '11-20/60';
                            case 3:
                                return '21-30/60';
                            case 4:
                                return '31-40/60';
                            case 5:
                                return '41-50/60';
                            case 6:
                                return '51-60/60';
                            default:
                                return '1-10/60';
                        }
                    }
                    navigatorEl.textContent = '';
                    const newNav = document.createElement('nav');
                    newNav.setAttribute('aria-label', 'Page navigation example');
                    newNav.innerHTML = `
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" mode="${pageNum - 1}" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">${pageId()}</a></li>
                        <li class="page-item">
                            <a class="page-link" mode="${pageNum + 1}" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                        <li class="page-item">
                    <a class="page-link" mode="all" aria-label="Next">
                        Show all
                    </a>
                </li>
                    </ul>`;
                    navigatorEl.appendChild(newNav);
                    if (pageNum === 1) {
                        document.querySelector('.pagination li').classList.add('disabled')
                    }
                    if (pageNum === 6) {
                        document.querySelectorAll('.pagination li')[2].classList.add('disabled')
                    }
                }
                return await getPage(urls[urlIndex]);
            })();
        }
    }
)
